#include <stdio.h>
#include <cs50.h>

void check_bank(int fn);
void check_lunh(long cc); 


int main(void){
  long credit_card = get_long("Number: ");
  check_lunh(credit_card);
  return 0;
}


void check_bank(int fn){
  switch(fn){
    case 34:
    case 37:
      printf("AMEX\n");
      break;
    case 51:
    case 52:
    case 53:
    case 54:
    case 55:
      printf("MASTERCARD\n");
      break;
    case 04:
      printf("VISA\n");
      break;
    default:
      printf("VALID\n");
}
}

void check_lunh(long cc){
  long n = cc;
  int result = 0;
  short digit = 1;
  short temp;
  short first_digits;
  do {
    temp = n % 10;
    if(digit % 2 == 0){
      temp *= 2;
    }
    if(temp > 10){
      temp = (temp / 10) + (temp % 10);
    }
    result += temp;
    digit++;
    n /= 10;
    if (n < 100 && n > 0){
      first_digits = n;
    }
  } while(n > 0);
  result %= 10;
  if(result != 0){
    printf("INVALID\n");
  } else {
    check_bank(first_digits);
  }
}
