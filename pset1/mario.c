#include <cs50.h>
#include <stdio.h>

// Prototypes of the functions.
void print_blackspaces(int n);
void print_hahes(int n);
int get_positive_int(string prompt, int max);
void mario_pyramid(int height);

int main(void){
    int height = get_positive_int("Height: ", 8);
    mario_pyramid(height);
    return 0;
}

// Function to print n amount of blackspaces
void print_blackspaces(int n){
    for(int i = 0; i < n; i++){
        printf(" ");
    }
}

// Function to print n amount of hashes ('#')
void print_hashes(int n){
    for(int i = 0; i < n; i++){
        printf("#");
    }
}

// Function to get only a positive integer
int get_positive_int(string prompt, int max){
    int number;
    do{
        number = get_int("%s", prompt);
    } while(number < 1 || number > max);
    return number;
}

// Function to print a Mario pyramid with 'height' height
void mario_pyramid(int height){
    int blackspaces;
    for(int i = 1; i <= height; i++){
        blackspaces = height - i;
        print_blackspaces(blackspaces);
        print_hashes(i);
        print_blackspaces(2);
        print_hashes(i);
        print_blackspaces(blackspaces);
        printf("\n");
    }
}
